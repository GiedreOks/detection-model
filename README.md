# Detection model

This project was a part of a university assignment - create a regression model that can detect a response variable (y) from a set of factors (X).

The data was anonymized and did not contain labels for features or the response variable hence the problem context is minimal.

## Linear regression using regularization methods

The model is comprised of three main scripts:

* src/main.py - the main modelling file.	

* split/split.py - script to split data to train/validation/test datasets.

* src/testing.py - script to test model accuracy.
	
You can also find example bash scripts under /src.

## Running the models
main.py script requires three parameters: output directory, a path to independent variables (X) and a path to the dependent variable (y).
Either run this in shell:
> 	`python clickas.py ./output ./raw/x.txt ./raw/y.txt`

or run bash script "job.bash" with parameters which you can find in the /src directory.

## Testing the model
Run this command in shell:

>   `python testing.py ./output L2 0.001`

or run bash script "test_job.bash" with parameters which you can find in the /src directory.